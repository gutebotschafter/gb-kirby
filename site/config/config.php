<?php

return [
    'mgfagency.twig.debug' => true,
    'mgfagency.twig.cache' => false,
    'mgfagency.twig.function.inlineRenderer' => 'inlineRenderer',
    'email' => [
        'transport' => [
            'type' => 'smtp',
            'host' => 's',
            'port' => 587,
            'auth' => true,
            'security' => 'tls',
            'username' => '',
            'password' => ''
        ]
    ]
];
