// import swRuntime from 'serviceworker-webpack-plugin/lib/runtime';

import './polyfills';

import { dummy } from './components/dummy';
// import loadChunks from './utils/load-chunks';

dummy();

/**
 * Activate to load chunks
 */
// loadChunks();

if ('serviceWorker' in navigator) {
    /**
     * Activate to register the service worker
     */
    // swRuntime.register();
}
